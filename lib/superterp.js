;(function(root) {
  "use strict";

  function delimToRe(d) {
    var str = '';
    for (var i = 0; i < d.length; i++) {
      str += '[' + d[i] + ']';
    }
    return str;
  }

  function withDelimiters(start, end) {
    var str =
      delimToRe(start) +
      '([\\s\\S]+?)' +
      delimToRe(end);
    return makeTerp({
      matchRe: new RegExp(str, "g")
    });
  }

  function makeTerp(options) {
    var f = function terp(str, vars, context) {
      if (!str || typeof str !== 'string') {
        return str;
      }
      vars = vars || {};

      return str.replace(options.matchRe, function(match, expression) {
        var f = new Function('__vars', 'with(__vars) { return ' + expression + ';}');
        return f.call(context, vars);
      });
    };

    f.withDelimiters = withDelimiters;

    return f;
  }

  var terp = withDelimiters("${", "}");

  // Export
  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = terp;
  } else {
    root.terp = terp;
  }
})(this);
