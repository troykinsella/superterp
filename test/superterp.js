var assert = require('assert');
var terp = require('../lib/superterp');

var t;

describe('superterp', function () {

  it('should gracefully handle no parameters', function() {
    assert.strictEqual(terp(), undefined);
  });

  it('should gracefully handle non-string values', function() {
    assert.strictEqual(undefined, terp(undefined));
    assert.strictEqual(terp(null), null);
    assert.strictEqual(terp(0), 0);
    assert.strictEqual(terp(1), 1);
    assert.strictEqual(terp(true), true);
    assert.strictEqual(terp(false), false);
    assert(terp(/re/) instanceof RegExp);
    assert.strictEqual(terp(/re/).toString(), /re/.toString());
    assert.deepEqual(terp([]), []);
    assert.deepEqual(terp({}), {});
  });

  it('should do nothing with plain strings', function() {
    assert.strictEqual(terp("shit"), "shit");
  });

  [ [ "${", "}" ],
    [ "{{", "}}" ],
    [ "<%", "%>" ],
    [ "<%=", "%>" ],
    [ "##", "##" ],
    [ "--", "--" ],
    [ "<<", ">>" ],
    [ "start", "end" ]].forEach(function(d) {

    describe("delimiters " + d.join(' '), function() {

      beforeEach(function() {
        t = terp.withDelimiters(d[0], d[1]);
      });

      it('should interpolate a full-string substitution', function() {
        assert.strictEqual(t(d[0] + "fuck" + d[1], { fuck: "shit" }), "shit");
      });

      it('should interpolate a starting substitution', function() {
        assert.strictEqual(
          t(d[0] + "fuck" + d[1] + "head", { fuck: "shit" }),
          "shithead");
      });

      it('should interpolate an ending substitution', function() {
        assert.strictEqual(
          t("ass" + d[0] + "fuck" + d[1], { fuck: "face" }),
          "assface");
      });

      it('should interpolate multiple full-string substitutions', function() {
        assert.strictEqual(
          t(d[0] + "fuck" + d[1] + d[0] + "shit" + d[1], { fuck: "ass", shit: "hole" }),
          "asshole");
        assert.strictEqual(
          t(d[0] + "fuck" + d[1] + d[0] + "shit" + d[1] + d[0] + "bastard" + d[1], { fuck: "ass", shit: "hole", bastard: "mouth" }),
          "assholemouth");
      });

      it('should interpolate multiple substitutions', function() {
        assert.strictEqual(t("hey " + d[0] + "name" + d[1] + " breath, you " + d[0] + "verb" + d[1], { name: "donkey", verb: "suck" }),
          "hey donkey breath, you suck");
      });

      it('should evaluate interpolated expressions', function() {
        assert.strictEqual(
          t(d[0] + "shit + ' up ' + fuck" + d[1], { shit: "head", fuck: "ass" }),
          "head up ass");
      });

      /*it('should gracefully handle partial interpolation delimiters', function() {
        assert.strictEqual(
          t("shit " + d[0] + " fuck"),
          "shit " + d[0] + " fuck");
        assert.strictEqual(
          t("shit " + d[1] + " fuck"),
          "shit " + d[1] + " fuck");
        assert.strictEqual(
          t(d[1] + " shit fuck " + d[0]),
          d[1] + " shit fuck " + d[0]);
      });*/

      it('should interpolate all substitution types', function() {
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: "you" }), "shit you");
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: 0 }), "shit 0");
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: 1 }), "shit 1");
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: 1.23 }), "shit 1.23");
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: true }), "shit true");
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: false }), "shit false");
        assert.strictEqual(t("shit " + d[0] + "fuck" + d[1], { fuck: /you/ }), "shit /you/");
        assert.strictEqual(t("shit " + d[0] + "fuck()" + d[1], { fuck: function() { return "you"; } }), "shit you");
      });

      it('should fail on missing interpolation key', function() {
        assert.throws(function() {
          t(d[0] + "shit" + d[1], { fuck: 'off' });
        }, ReferenceError);
      });

      it('should gracefully handle a missing interpolation value', function() {
        assert.strictEqual(t(d[0] + "shit" + d[1] + " fuck", { shit: undefined }), "undefined fuck");
      });

      it('should interpolate against a given context', function() {
        var context = {
          shit: "head"
        };
        assert.strictEqual(t(d[0] + "fuck" + d[1] + d[0] + "this.shit" + d[1], { fuck: "shit" }, context), "shithead");
      });

    });

  });

  it('should support delimiter escape pattern', function() {
    assert.strictEqual("${fuck}", terp("${'$'}{fuck}", { fuck: "shit" }));
    assert.strictEqual("Emoticons ${ and moustaches and shit.", terp("Emoticons ${'$'}{ and moustaches and shit."));
  });

  /*it('should evaluate complex expressions', function() {
    assert.strictEqual("shit fuck", terp("shit ${(function() { return 'fuck'; })()}"));
  });*/

});
