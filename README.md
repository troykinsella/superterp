# superterp [![npm version](https://badge.fury.io/js/superterp.svg)](http://badge.fury.io/js/superterp)
> A super lightweight simple string inTERPolation library for javascript and node.js

[![NPM](https://nodei.co/npm/superterp.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/superterp/)
[![NPM](https://nodei.co/npm-dl/superterp.png?months=6&height=3)](https://nodei.co/npm/superterp/)

## Install

### Node.js

```sh
$ npm install --save superterp
```

### Vanilla JavaScript

Copy the distribution superterp.js file into your project files, and include it as per usual:
```html
<script src="superterp.js"></script>
```

## Usage

```js
> var terp = require('superterp'); // Node.js

> terp('Hi ${name}! You have ${coolness} hair!', { name: 'Reginald', coolness: 'sweet' });
'Hi Reginald! You have sweet hair!'
```

### terp([str], [vars], [context])

Parameters:

* str - The source String to be inTERPolated. Optional.
* vars - An Object defining variables that will be available to expressions embedded in the source `str`. Optional.
* context - The Object that will become `this` in embedded expressions in the source `str`. Optional.

`terp` returns the String result of the inerpolation. If the `str` parameter isn't a String, 
the value of `str` is returned without modification.

## Examples

Use the full power of javascript expressions in substitutions:
```
> terp("${name} smokes ${joints + 10} joints a day.", { name: "Ted", joints: 4 });
'Ted smokes 14 joins a day.'
```

Evaluate interpolations against a context object:
```
> var ted = { name: "Ted" };
> var turd = { name: "Turd" };
> var template = "Who likes ${food}? ${this.name} does.";
> terp(template, { food: "liver" }, ted);
'Who likes liver? Ted does.'
> terp(template, { food: "bees" }, turd);
'Who likes bees? Turd does.'
```

Alternate expression delimiters:
```js
> terp = terp.withDelimiters("{{", "}}");
> terp('{{name}} ate {{num}} sandwiches!', { name: 'Chad', num: 14 });
'Chad ate 14 sandwiches!'
```

Escape a conflicting opening interpolation delimiter:
```
> terp("Emoticons ${'$'}{ and moustaches and shit.")
'Emoticons ${ and moustaches and shit.'
```

## Limitations

superterp aims to be lighweight. It doesn't make attempts to support nested occurrences of the expression delimiters.
Specifically, you can't put a `}` anywhere inside a `${ ... }`. Deal with it.

## Testing

```sh
gulp test
```

### Roadmap

* Generate minfied vanilla JavaScript distributable
* Bower component

## License

MIT © [Troy Kinsella]()
